## **Algorithm Samples**

#**Contents**

##1. Credit Card Masker
`For given inputs such as`
 1234-4567-9874-5632 `it will generate safe masked number as`
 1###-####-####-5632
 `in general will mask out any digits excepts 1st and last 4`
 

##2. RPN - Reverse polish notation --
 https://en.wikipedia.org/wiki/Reverse_Polish_notation
 
Accepts string having numbers and operators (+,-,*,/) and calculates results for it
`Example: for input [4 2 /] results will be 2 and [1 3 +] will be 4`

##3. Ordinal
For any any given number it returns ordinal of the numbers, 
`such as 1st for 1, 3rd for 3, 100th for 100 etc.`

##4. Merge Sort Algorithm


##5. FizzBuzz problem
https://en.wikipedia.org/wiki/Fizz_buzz

Solved using 3 different approaches calibrating performance, having following results-

```
 Loop Limit = 100000
 Time taken for startFizzBuzzBeginner: 1806 milliseconds
 Time taken for startFizzBuzzIntermediate: 1688 milliseconds
 Time taken for startFizzBuzzAdvanced: 1298 milliseconds`

