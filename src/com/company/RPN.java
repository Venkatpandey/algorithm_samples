package com.company;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Stack;

public class RPN {
	public static void main(String[] args) {
		System.out.println(evaluate("5 1 2 + 4 * 3 -"));
	}

	public static double evaluate(String expr) {
		String[] input = expr.trim().split(" ");
		Stack<BigDecimal> calc = new Stack<BigDecimal>();
		for (String s : input) {
			if (s.equals("")) {
				return 0;
			} else if (isNumeric(String.valueOf(s))) {
				calc.push(BigDecimal.valueOf(Double.parseDouble(s)));
			} else {
				BigDecimal inputF = calc.pop();
				BigDecimal inputS = calc.pop();
				switch (s) {
					case "+":
						calc.push(inputS.add(inputF));
						break;
					case "-":
						calc.push(inputS.subtract(inputF));
						break;
					case "*":
						calc.push(inputS.multiply(inputF));
						break;
					case "/":
						calc.push(inputS.divide(inputF, RoundingMode.HALF_EVEN));
						break;
				}
			}
		}
		return calc.pop().doubleValue();
	}

	public static boolean isNumeric(String strNum) {
		if (strNum == null) {
			return false;
		}
		try {
			double d = Double.parseDouble(strNum);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}
}
