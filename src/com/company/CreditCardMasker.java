package com.company;

public class CreditCardMasker {
	public static void main(String[] args) {

		String ccnumber = "1234567891234567";

		int length = ccnumber.length();
		int start = 1;
		int end = 4;

		String to_replace = ccnumber.substring(1, length - 4);
		char[] chars = to_replace.toCharArray();
		StringBuilder mask = new StringBuilder();
		for (char c : chars) {
			if (isNumeric(String.valueOf(c))) {
				mask.append("#");
			} else {
				mask.append(c);
			}
		}

		String finalNumber = ccnumber.substring(0,1).concat(mask.toString()).concat(ccnumber.substring(length-4, length));
		System.out.println(finalNumber); // prints 1###########4567
	}
	public static boolean isNumeric(String strNum) {
		if (strNum == null) {
			return false;
		}
		try {
			double d = Double.parseDouble(strNum);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}
}
