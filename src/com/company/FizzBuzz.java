package com.company;

import java.time.Duration;
import java.time.Instant;

public class FizzBuzz {

	static int LIMIT = 100000;

	public static void main(String[] args) {
		Instant beggStart = Instant.now();
		startFizzBuzzBeginner();
		Instant beggEnd = Instant.now();
		Duration startFizzBuzzBeginner = Duration.between(beggStart, beggEnd);

		Instant interStart = Instant.now();
		startFizzBuzzIntermediate();
		Instant interEnd = Instant.now();
		Duration startFizzBuzzIntermediate = Duration.between(interStart, interEnd);

		Instant advanceStart = Instant.now();
		startFizzBuzzAdvanced();
		Instant advanceEnd = Instant.now();
		Duration startFizzBuzzAdvanced = Duration.between(advanceStart, advanceEnd);

		System.out.println("Time taken for startFizzBuzzBeginner: "+ startFizzBuzzBeginner.toMillis() +" milliseconds");
		System.out.println("Time taken for startFizzBuzzIntermediate: "+ startFizzBuzzIntermediate.toMillis() +" milliseconds");
		System.out.println("Time taken for startFizzBuzzAdvanced: "+ startFizzBuzzAdvanced.toMillis() +" milliseconds");

	}

	private static void startFizzBuzzAdvanced() {

		int three = 0;
		int five = 0;

		for (int i = 0 ; i <=LIMIT ; i++) {
			three++;
			five++;
			String data = "";
			if (three == 3) {
				data = data.concat("Fizz");
				three = 0;
			}
			if (five == 5) {
				data = data.concat("Buzz");
				five = 0;
			}

			if (data.equals("")) {
				System.out.println(i);
			} else {
				System.out.println(data);
			}
		}
	}

	private static void startFizzBuzzIntermediate() {
		for (int i = 0 ; i <=LIMIT ; i++) {
			String data = "";
			if (i % 3 == 0) { data = data.concat("Fizz"); }
			if (i % 5 == 0) { data = data.concat("Buzz"); }
			if (data.equals("")) {
				System.out.println(i);
			} else {
				System.out.println(data);
			}
		}

	}

	private static void startFizzBuzzBeginner() {
		for (int i = 0 ; i <=LIMIT ; i++) {
			if (i % 15 == 0) {
				System.out.println("FizzBuzz");
			} else if (i % 3 == 0) {
				System.out.println("Fizz");
			} else if (i % 5 == 0) {
				System.out.println("Buzz");
			} else {
				System.out.println(i);
			}
		}
	}
}
