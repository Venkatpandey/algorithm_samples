package com.company;

class Ordinal {
	public static void main(String[] args) {
		System.out.println(numberToOrdinal(2)); // prints 2nd
		System.out.println(numberToOrdinal(13)); // prints 13th
		System.out.println(numberToOrdinal(3)); // prints 3rd
		System.out.println(numberToOrdinal(1)); // prints 1st
	}
	public static String numberToOrdinal( Integer number ) {
		String ordinal = null;
		if (number == 0) {
			return "0";
		}
		int last1 = number % 10;
		int last2 = number % 100;


		if (last1 == 1 && last2 != 11) {
			ordinal = Integer.toString(number).concat("st");
		} else if (last1 == 2 && last2 != 12) {
			ordinal = Integer.toString(number).concat("nd");
		} else if (last1 == 3 && last2 != 13) {
			ordinal = Integer.toString(number).concat("rd");
		} else {
			ordinal = Integer.toString(number).concat("th");
		}

		return ordinal;

	}
}

